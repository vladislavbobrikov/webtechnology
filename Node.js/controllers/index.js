var templEngine = require('../lib/templEngine');
var models = require('../models');
var style = require('fs');
var jquery = require('fs');
var script = require('fs');
var database = require('fs');

exports.indexController = function(req, res){
    var stud = new models.Student("qwe").all(
        function(data){
            var convertedData = [];
            data.forEach(function(element) {
                convertedData.push(element.name + ' ' + element.surname + ' ' + element.patronymic + ' ' + element.course + ' ' + element.record_book);
            }, this);
            templEngine.view('./views/index.html', res, convertedData);
        }
    );  
}

exports.notFound = function(req, res){
    templEngine.view('./views/notFound.html', res);
}

exports.css = function(req, res){
    style.readFile('./views/style.css', 'utf-8',function(err, data){
        res.end(data);
    });
}

exports.jquery = function(req, res){
    jquery.readFile('./views/jquery-3.2.1.min.js', 'utf-8',function(err, data){
        res.end(data);
    });
}

exports.script = function(req, res){
    script.readFile('./views/script.js', 'utf-8',function(err, data){
        res.end(data);
    });
}

exports.database = function(req, res){
    database.readFile('./db/db.json', 'utf-8',function(err, data){
        res.end(data);
    });
}


