var fs = require('fs');

exports.view = function(file, res, viewdata){
    fs.readFile(file, 'utf-8', function(err, data){
        if(err){
            res.end('Template engine error!');
        }
        else{
            if(viewdata){
                var html = data.replace('%', viewdata.join('<li></li>'));
                res.end(html.toString());
            }
            else{
                res.end(data.toString());
            }
        }
    });
}