var fs = require('fs');

exports.Student = function(name, surname, patronymic, course, record_book){
    this.name = name;
    this.surname = surname;
    this.patronymic = patronymic;
    this.course = course;
    this.record_book = record_book;

    this.all = function(callback){
        fs.readFile('./db/db.json', 'utf-8',function(err, data){
            callback(JSON.parse(data.toString()));
        });


    }
}