function Person (name, surname, patronymic) {
	this.name = name;
	this.surname = surname;
	this.patronymic = patronymic;
}

function Student (name, surname, patronymic, course, record_book) {
	Person.call(this, name, surname, patronymic);
	this.course = course;
    this.record_book = record_book;
}

Student.prototype = Person.prototype;
Student.prototype.constructor = Student;

$(document).ready(function(){

	var isNameValid = false, isSurnameValid = false, isPatronymicValid = false, isCourseValid = false, isRecord_bookValid = false;

	submitControl();
	function submitControl(){
		if(isNameValid && isSurnameValid && isPatronymicValid && isCourseValid, isRecord_bookValid)
			$('#submit').prop("disabled", false);
		else
			$('#submit').prop("disabled", true);
	}

	$('#name').on('input keyup', function() {
		if($('#name').val() === '') {
			isNameValid = false;
			$('#name').removeClass('success').addClass('error');
		}
		else {
			isNameValid = true;
			$('#name').removeClass('error').addClass('success');
		}
		submitControl();
	});


	$('#surname').on('input keyup', function() {
		if($('#surname').val() === '') {
			isSurnameValid = false;
			$('#surname').removeClass('success').addClass('error');
		}
		else {
			isSurnameValid = true;
			$('#surname').removeClass('error').addClass('success');
		}
		submitControl();
	});

	$('#patronymic').on('input keyup', function() {
		if($('#patronymic').val() === '') {
			isPatronymicValid = false;
			$('#patronymic').removeClass('success').addClass('error');
		}
		else {
			isPatronymicValid = true;
			$('#patronymic').removeClass('error').addClass('success');
		}
		submitControl();
	});

	$('#course').on('input keyup', function() {
		if($('#course').val() === '') {
			isCourseValid = false;
		}
		else {
			isCourseValid = true;
		}
		submitControl();
	});
    
    $('#record_book').on('input keyup', function() {
		if($('#record_book').val() === '') {
			isRecord_bookValid = false;
		}
		else {
			isRecord_bookValid = true;
		}
		submitControl();
	});

	$('#submit').click(function(e){
		e.preventDefault();
		var newStudent = new Student(
			$('#name').val(),
			$('#surname').val(),
			$('#patronymic').val(),
			$('#course').val(),
            $('#record_book').val(),
		);
		$('<div class="container">' + '<li>' + newStudent.name + " " + newStudent.surname + " " + newStudent.patronymic + " " + newStudent.course + " " + newStudent.record_book + '</li>' + '<button id="btn">x</button>' + '<div class="clear"></div>' + '</div>').hide().appendTo('#students_list').fadeIn(1000);
		
	});

	$(document).on("click", "#btn", function(e) {
		e.preventDefault();
		$(this).parent().remove();
	});

});
