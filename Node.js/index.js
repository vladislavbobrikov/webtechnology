var http = require('http');
var router = require('./lib/router');

http.createServer(function(req, res){
    router.router(req, res);
}).listen(3000, () => console.log('Server running'));