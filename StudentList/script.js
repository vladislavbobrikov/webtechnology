function Person (name, surname, patronymic) {
	this.name = name;
	this.surname = surname;
	this.patronymic = patronymic;
}

function Student (name, surname, patronymic, course, record_book) {
	Person.call(this, name, surname, patronymic);
	this.course = course;
    this.record_book = record_book;
}

Student.prototype = Person.prototype;
Student.prototype.constructor = Student;

window.onload = function() {
	var nameField = document.getElementById('name');
	var surnameField = document.getElementById('surname');
	var patronymicField = document.getElementById('patronymic');
	var courseField = document.getElementById('course');
    var record_bookField = document.getElementById('record_book');
	var submitButton = document.getElementById('submit');
	var isNameValid = false, isSurnameValid = false, isPatronymicValid = false, isCourseValid = false, isRecord_book = false;
	var StudentList = document.getElementById('student_list');
	var list = [];

	submitControl();
	function submitControl(){
		if(isNameValid && isSurnameValid && isPatronymicValid && isCourseValid && isRecord_book)
			submitButton.disabled = false;
		else
			submitButton.disabled = true;
	}

	nameField.oninput = function() {
		if(nameField.value === '') {
			isNameValid = false;
			nameField.className = 'error';
		}
		else {
			isNameValid = true;
			nameField.className = 'success';
		}
		submitControl();
	}

	surnameField.oninput = function() {
		if(surnameField.value === '') {
			isSurnameValid = false;
			surnameField.className = 'error';
		}
		else {
			isSurnameValid = true;
			surnameField.className = 'success';
		}
		submitControl();
	}

	patronymicField.oninput = function() {
		if(patronymicField.value === '') {
			isPatronymicValid = false;
			patronymicField.className = 'error';
		}
		else {
			isPatronymicValid = true;
			patronymicField.className = 'success';
		}
		submitControl();
	}

	courseField.oninput = function() {
		if(courseField.value === '') {
			isCourseValid = false;
			courseField.className = 'error';
		}
		else {
			isCourseValid = true;
			courseField.className = 'success';
		}
		submitControl();
	}
    
    record_bookField.oninput = function() {
        if(record_bookField.value === '') {
            isRecord_book = false;
            record_bookField.className = 'error';
        }
        else {
            isRecord_book = true;
            record_bookField.className = 'success';
        }
        submitControl();
    }


	submitButton.onclick = function(event) {
		event.preventDefault();	
		list.push(new Student(nameField.value, surnameField.value, patronymicField.value, courseField.value, record_bookField.value));
		var elem = document.createElement('li');
		elem.innerHTML = list[list.length - 1].name + " " + list[list.length - 1].surname + " " + list[list.length - 1].patronymic + " " + list[list.length - 1].course + " " + list[list.length - 1].record_book;
		StudentList.appendChild(elem);

		

         var stud = new Student(nameField.value, surnameField.value, patronymicField.value, courseField.value, record_bookField.value);
		 console.log(stud);

	}
}